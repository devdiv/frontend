#/bin/bash

SERVICE=${1}
CWD=$(cd .. && pwd)

if [[ $SERVICE == "frontend" ]]
then
  echo "replace $SERVICE service"
  docker stop $SERVICE && docker rm $SERVICE && docker image rm frontend_$SERVICE
  cd $CWD/$SERVICE/frontend && npm run build

elif [[ $SERVICE == "game" ]]
then
  echo "replace $SERVICE service"
  docker stop $SERVICE && docker rm $SERVICE && docker image rm frontend_$SERVICE

  cd $CWD/${SERVICE}service/backend && mvn clean install

elif [[ $SERVICE == "flagr" ]]
then
  echo "replace $SERVICE service"
  docker stop $SERVICE && docker rm $SERVICE

elif [[ $SERVICE == "mysql" ]]
then
  echo "replace $SERVICE service"
  docker stop $SERVICE flagr flashcards && docker rm $SERVICE flashcards

elif [[ $SERVICE == "flashcard" ]]
then
  echo "replace $SERVICE service"
  docker stop ${SERVICE}s && docker rm ${SERVICE}s && docker image rm frontend_${SERVICE}s

  cd $CWD/${SERVICE}service/backend && mvn clean install

elif [[ -z $SERVICE ]]
then
  echo 'Starting all services'
  cd $CWD/frontend/frontend && npm run build
  cd $CWD/gameservice/backend && mvn clean install
  cd $CWD/flashcardservice/backend && mvn clean install

else
  echo 'Either specify an argument of "game", "flashcard", or "frontend" to replace that specific service, or leave empty to start fresh (not replace!) all services'
fi

cd $CWD/frontend && docker-compose -f docker-compose.local-dev.yaml up -d
if [[ $SERVICE == "mysql" || $SERVICE == "flagr" || -z $SERVICE ]]
then
  sleep 5
  bash create-flags.sh
fi
