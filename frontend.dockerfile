FROM maven:3.8-jdk-11 as build
RUN apt-get update && apt-get install -y curl sudo

# Node
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash - && \
  sudo apt-get install -y nodejs && \
  echo "NODE Version:" && node --version && \
  echo "NPM Version:" && npm --version

# copy code and run build
WORKDIR /src
COPY ./sources/frontend/mosar-frontend/*.json ./frontend/mosar-frontend/
COPY ./sources/openapi ./openapi
COPY ./sources/frontend/mosar-frontend/src ./frontend/mosar-frontend/src
RUN cd ./frontend/mosar-frontend && npm install && npm run build

# run frontedn with nginx reverse proxy
FROM nginx:stable-alpine as runtime
COPY --from=build /src/frontend/mosar-frontend/dist/mosar-frontend /usr/share/nginx/html
WORKDIR configs
COPY ./sources/frontend/mosar-frontend/nginx-aws-test.conf ./test.conf
COPY ./sources/frontend/mosar-frontend/nginx-aws-prod.conf ./prod.conf
COPY ./sources/frontend/mosar-frontend/nginx-local.conf ./local.conf
COPY ./sources/frontend/mosar-frontend/start-frontend.sh .
EXPOSE 80
CMD [ "sh", "start-frontend.sh" ]

