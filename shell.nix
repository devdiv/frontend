{ pkgs ? import <nixpkgs> { } }:
let unstable = import <nixos-unstable> {};
#    cypress = unstable.callPackage ./cypress.nix {};
in
pkgs.mkShell {
  buildInputs = [
    unstable.jdk11
    unstable.maven
    unstable.mysql
    unstable.nodejs-17_x
    unstable.awscli2
    unstable.python27
    unstable.yarn
    unstable.sass
    unstable.terraform
  ];
  shellHook = ''
    export CYPRESS_INSTALL_BINARY=0
    export CYPRESS_RUN_BINARY=${unstable.cypress}/bin/Cypress
  '';
}

