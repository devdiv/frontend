#/bin/bash

echo ""
echo "deleting default flag"
curl -X DELETE -H "Content-Type: application/json" \
    http://localhost:18000/api/v1/flags/1

echo ""
echo "creating flag"
curl -X POST -H "Content-Type: application/json" \
    -d '{"description": "psm", "key": "psm"}' \
    http://localhost:18000/api/v1/flags

echo ""
echo "enable the flag"
curl -X PUT -H "Content-Type: application/json" \
    -d '{"enabled": true}' \
    http://localhost:18000/api/v1/flags/2/enabled

echo ""
echo "creating featureEnabled variant"
curl -X POST -H "Content-Type: application/json" \
    -d '{"key": "featureEnabled"}' \
    http://localhost:18000/api/v1/flags/2/variants

echo ""
echo "creating featureDisabled variant"
curl -X POST -H "Content-Type: application/json" \
    -d '{"key": "featureDisabled"}' \
    http://localhost:18000/api/v1/flags/2/variants

echo ""
echo "creating segment allUsers"
curl -X POST -H "Content-Type: application/json" \
    -d '{"description": "allUsers", "rolloutPercent":100}' \
    http://localhost:18000/api/v1/flags/2/segments

echo ""
echo "creating distributions"
curl -X PUT -H "Content-Type: application/json" \
    -d '{"distributions": [{"percent":0, "variantKey": "featureEnabled", "variantID":4}, {"percent":100, "variantKey": "featureDisabled", "variantID":5}]}' \
    http://localhost:18000/api/v1/flags/2/segments/2/distributions
