pipeline {
  agent {
    label 'mosar_terraform'
  }

  environment {
    BUCKET_FRONTEND = "${BUCKET_FRONTEND}"
  }
  post {
    failure {
      updateGitlabCommitStatus name: 'jenkins', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'jenkins', state: 'success'
    }
  }
  options {
    gitLabConnection('devidiv-gitlab-connection')
    gitlabBuilds(builds: ['terraform-apply-test', 'terraform-apply-prod'])
  }

  stages {
      stage ('terraform-apply-test') {
        environment {
          ENVIRONMENT        = "test"
          TF_VAR_environment = "test"
        }

        steps {
          echo "This was a merge event. Now deploying to test"
          sh "mkdir -p ./frontend/tf && cp -r ./frontend/terraform ./frontend/tf/${ENVIRONMENT}"
          sh "cd ./frontend/tf/${ENVIRONMENT} && aws s3 cp 's3://${BUCKET_FRONTEND}/${ENVIRONMENT}/terraform_files/terraform.tfvars' terraform.tfvars"
          sh "cd ./frontend/tf/${ENVIRONMENT} && aws s3 cp 's3://${BUCKET_FRONTEND}/${ENVIRONMENT}/terraform_files/providers.tf' providers.tf"
          sh "cd ./frontend/tf/${ENVIRONMENT} && aws s3 cp 's3://${BUCKET_FRONTEND}/${ENVIRONMENT}/terraform_plans/latest.tfplan' latest.tfplan"
          sh "cd ./frontend/tf/${ENVIRONMENT} && rm providers-template.tf && rm terraform-template.tfvars"
          sh "cd ./frontend/tf/${ENVIRONMENT} && terraform init"
          // sh "cd ./frontend/tf/${ENVIRONMENT} && terraform taint \'module.ecs-app[\"frontend\"].aws_ecs_service.service\' || true"
          // sh "cd ./frontend/tf/${ENVIRONMENT} && terraform taint \'module.ecs-app[\"frontend\"].aws_ecs_task_definition.task_definition\' || true"
          sh "cd ./frontend/tf/${ENVIRONMENT} && terraform apply -auto-approve latest.tfplan"
        }
        post {
          success {
            updateGitlabCommitStatus name: 'terraform-apply-test', state: 'success'
          }
          failure {
            updateGitlabCommitStatus name: 'terraform-apply-test', state: 'failed'
          }
        }
      }
      stage ('terraform-apply-prod') {
        environment {
          ENVIRONMENT        = "prod"
          TF_VAR_environment = "prod"
        }
        options {
          timeout(time: 15, unit: 'MINUTES')
        }
        input {
          message "Deploy to Production ?"
        }
        steps {
          echo "Deployment approved. Now deploying to prod"
          sh "mkdir -p ./frontend/tf && cp -r ./frontend/terraform ./frontend/tf/${ENVIRONMENT}"
          sh "cd ./frontend/tf/${ENVIRONMENT} && aws s3 cp 's3://${BUCKET_FRONTEND}/${ENVIRONMENT}/terraform_files/terraform.tfvars' terraform.tfvars"
          sh "cd ./frontend/tf/${ENVIRONMENT} && aws s3 cp 's3://${BUCKET_FRONTEND}/${ENVIRONMENT}/terraform_files/providers.tf' providers.tf"
          sh "cd ./frontend/tf/${ENVIRONMENT} && aws s3 cp 's3://${BUCKET_FRONTEND}/${ENVIRONMENT}/terraform_plans/latest.tfplan' latest.tfplan"
          sh "cd ./frontend/tf/${ENVIRONMENT} && rm providers-template.tf && rm terraform-template.tfvars"
          sh "cd ./frontend/tf/${ENVIRONMENT} && terraform init"
          // sh "cd ./frontend/tf/${ENVIRONMENT} && terraform taint \'module.ecs-app[\"frontend\"].aws_ecs_service.service\' || true"
          // sh "cd ./frontend/tf/${ENVIRONMENT} && terraform taint \'module.ecs-app[\"frontend\"].aws_ecs_task_definition.task_definition\' || true"
          sh "cd ./frontend/tf/${ENVIRONMENT} && terraform apply -auto-approve latest.tfplan"
        }
        post {
          success {
            updateGitlabCommitStatus name: 'terraform-apply-prod', state: 'success'
          }
          failure {
            updateGitlabCommitStatus name: 'terraform-apply-prod', state: 'failed'
          }
        }
      }
   }
}

