FROM nginx:stable-alpine as runtime
COPY dist/mosar-frontend /usr/share/nginx/html
WORKDIR configs
COPY nginx-aws-test.conf ./test.conf
COPY nginx-aws-prod.conf ./prod.conf
COPY nginx-local.conf ./local.conf
COPY start-frontend.sh .
EXPOSE 80
CMD [ "sh", "start-frontend.sh" ]

