[
{
  "name": "${CONTAINER_NAME}",
    "image": "${FRONTEND_IMAGE}:${FRONTEND_IMAGE_TAG}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "mosar-frontend-${ENVIRONMENT}-service",
        "awslogs-group": "${MOSAR_LOG_GROUP_NAME}"
      }
    },
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80,
        "protocol": "tcp"
      }
    ],
    "cpu": 256,
    "environment": [
      {
        "name": "ENVIRONMENT",
        "value": "${ENVIRONMENT}"
      }
    ],
    "secrets": [
    ],
    "ulimits": [
    ],
    "mountPoints": [],
    "memory": 512,
    "volumesFrom": []
  }
]
