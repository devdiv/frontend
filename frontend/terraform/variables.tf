# frontend app/variables.tf

variable "environment" {}
variable "frontend_image" {}
variable "frontend_image_tag" {}
variable "log_group_name" {}
variable "managed_by" {
  default = "jenkins_pipeline_frontend"
}
