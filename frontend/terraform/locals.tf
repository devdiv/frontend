locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  roles = {
    frontend_task = {
      role = {
        name               = "frontend-ecs-task-role_${var.environment}"
        description        = "frontend role tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "frontend-ecs-task-policy_${var.environment}"
        description = "Policy for Game task role"
        policy      = file("./iam_spec_files/frontend_ecs_task_policy.json")
      }
      policy_attachment = {
        name = "frontend-ecs-task-policy-attachment"
      }
    }
    frontend_task_execution = {
      role = {
        name               = "frontend-ecs-task-execution-role_${var.environment}"
        description        = "role to execute the frontend ecs tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "frontend-ecs-task-execution-policy_${var.environment}"
        description = "Policy to execute ecs frontend tasks"
        policy      = file("${path.root}/iam_spec_files/frontend_ecs_task_execution_policy.json")
      }
      policy_attachment = {
        name = "frontend-ecs-task-execution-policy-attachment"
      }
    }
  }
}

locals {
  security_groups = {
    frontend = {
      name         = "frontend_sg"
      vpc_id       = data.aws_vpc.mosar.id
      description  = "security group access to the frontend service"
      ingress_cidr = {}
      ingress_sg = {
        http = {
          from            = local.frontend_names.container_port
          to              = local.frontend_names.container_port
          protocol        = "tcp"
          security_groups = [data.aws_security_group.mosar_lb.id]
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  frontend_names = {
    name           = "frontend"
    container_name = "frontend-${var.environment}"
    namespace_id   = regex("/(?P<id>.*)", data.aws_route53_zone.zone.linked_service_description).id
    namespace_name = data.aws_route53_zone.zone.name
    container_port = 80
  }
}


locals {
  apps = {
    frontend = {
      name                    = "mosar-frontend"
      desired_count           = 1
      container_definitions   = templatefile("./mosar-frontend-container.tpl", local.container_vars)
      subnet_ids              = local.subnets.private
      security_groups         = module.security_groups.security_groups["frontend"].*.id
      assign_public_ip        = false
      family                  = "mosar-frontend-family"
      use_load_balancer       = true
      lb_target_group_arn     = data.aws_lb_listener.lb_listener.default_action[0].target_group_arn
      lb_container_name       = local.frontend_names.container_name
      lb_container_port       = local.frontend_names.container_port
      ecs_task_execution_role = module.iam.role["frontend_task_execution"]
      ecs_task_role           = module.iam.role["frontend_task"]
      cpu                     = 256
      memory                  = 512
      cluster                 = data.aws_ecs_cluster.cluster
      discovery_service = {
        name              = local.frontend_names.name
        namespace_id      = local.frontend_names.namespace_id
        ttl               = 30
        failure_threshold = 5
      }
      autoscaling = {
        max_capacity = 3
        min_capacity = 1
      }
      autoscaling_policies = {
        memory = {
          name                   = "frontend_memory_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageMemoryUtilization"
          target_value           = 80
        }
        cpu = {
          name                   = "frontend_cpu_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageCPUUtilization"
          target_value           = 60
        }
      }
    }
  }
}

locals {
  container_vars = {
    FRONTEND_IMAGE       = var.frontend_image
    FRONTEND_IMAGE_TAG   = var.frontend_image_tag
    CONTAINER_NAME       = local.frontend_names.container_name
    ENVIRONMENT          = var.environment
    MOSAR_LOG_GROUP_NAME = "mosar-logs-${var.environment}"
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}

locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}

