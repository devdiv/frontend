import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { Environment } from './environment.interface';
import { ApiModule as FlascardApiModule } from '../../build/openapi/flashcardservice/api/api.module';
import { ApiModule as GameApiModule } from '../../build/openapi/gameservice/api/api.module';

declare let __config: Environment;

const baseUrl = __config.baseUrl;

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FlascardApiModule.forRoot({
      rootUrl: `${baseUrl}/api/flashcards`,
    }),
    GameApiModule.forRoot({
      rootUrl: `${baseUrl}/api/game`,
    }),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
