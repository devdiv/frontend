import { ButtonHomeComponent } from './button-home/button-home.component';
import { ButtonNegativeComponent } from './button-negative/button-negative.component';
import { ButtonPositiveComponent } from './button-positive/button-positive.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [ButtonPositiveComponent, ButtonNegativeComponent, ButtonHomeComponent],
  imports: [CommonModule],
  exports: [ButtonNegativeComponent, ButtonPositiveComponent, ButtonHomeComponent],
})
export class ButtonsModule {}
