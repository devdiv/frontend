import { GameSequence } from '@generated/gameservice/api/models';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  public newSequenceState$ = new ReplaySubject<GameSequence>(1);
  public courseId$ = new ReplaySubject<number>(1);
  private readonly sequenceKey = 'sequence';
  private readonly courseIdKey = 'courseId';
  private _sequenceState: GameSequence | null = null;
  private _courseId = -1;

  constructor() {
    this.newSequenceState$.subscribe((state) => localStorage.setItem(this.sequenceKey, JSON.stringify(state)));
    this.courseId$.subscribe((courseId) => {
      localStorage.setItem(this.courseIdKey, courseId.toString());
    });
  }

  get sequenceState(): GameSequence | null {
    if (this._sequenceState !== null) {
      return this._sequenceState;
    }
    this._sequenceState =
      localStorage.getItem(this.sequenceKey) != null ? JSON.parse(localStorage.getItem(this.sequenceKey) ?? '') : null;
    return this._sequenceState;
  }

  get courseId(): number {
    if (this._courseId > -1) {
      return this._courseId;
    }
    this._courseId =
      localStorage.getItem(this.courseIdKey) != null ? JSON.parse(localStorage.getItem(this.courseIdKey) ?? '') : -1;
    return this._courseId;
  }

  clearSequenceState(): void {
    this._sequenceState = null;
    localStorage.removeItem(this.sequenceKey);
    this._courseId = -1;
    localStorage.removeItem(this.courseIdKey);
  }
}
