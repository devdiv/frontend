import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { GameSequence } from '@generated/gameservice/api/models';

import { StateService } from './state.service';

interface Spies {
  [key: string]: jest.SpyInstance;
}

describe('StateService', () => {
  let spectator: SpectatorService<StateService>;

  const createService = createServiceFactory({
    service: StateService,
  });

  const spies: Spies = {};

  const mockGameSequence: GameSequence = {
    courseid: '0',
    sequence: [3, 2, 2],
  };

  beforeEach(() => {
    spectator = createService();
    ['setItem', 'getItem', 'removeItem'].forEach((fn: string) => {
      const mock = jest.fn(localStorage[fn]);
      spies[fn] = jest.spyOn(Storage.prototype, fn).mockImplementation(mock);
    });
  });

  afterEach(() => {
    Object.keys(spies).forEach((key: string) => spies[key].mockRestore());
  });

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should set the sequence in localstorage if a new item was found', () => {
    spectator.service.newSequenceState$.next(mockGameSequence);
    expect(JSON.parse(localStorage.getItem('sequence')!) as GameSequence).toEqual(mockGameSequence);
    expect(spies.setItem).toHaveBeenCalledWith('sequence', JSON.stringify(mockGameSequence));
  });

  it('should set the courseId in localstorage if a new item was found', () => {
    spectator.service.courseId$.next(1);
    expect(JSON.parse(localStorage.getItem('courseId')!)).toEqual(1);
    expect(spies.setItem).toHaveBeenCalledWith('courseId', JSON.stringify(1));
  });

  it('should retrieve the sequence from the store if private variable is null', () => {
    expect(spectator.service['_sequenceState']).toBeNull();
    localStorage.setItem('sequence', JSON.stringify(mockGameSequence));
    const actual = spectator.service.sequenceState;
    expect(spies.getItem).toHaveBeenCalled();
    expect(actual).toEqual(mockGameSequence);
  });

  it('should retrieve the courseId from the store if private variable is null', () => {
    expect(spectator.service['_courseId']).toEqual(-1);
    localStorage.setItem('courseId', JSON.stringify(1));
    const actual = spectator.service.courseId;
    expect(spies.getItem).toHaveBeenCalled();
    expect(actual).toEqual(1);
  });

  it('should retrieve the sequence private variable if not null and not call getItem', () => {
    localStorage.setItem('sequence', JSON.stringify(mockGameSequence));
    let actual = spectator.service.sequenceState;
    expect(spectator.service['_sequenceState']).toEqual(mockGameSequence);
    spies.getItem.mockRestore();
    actual = spectator.service.sequenceState;
    expect(spies.getItem).not.toHaveBeenCalled();
    expect(actual).toEqual(mockGameSequence);
  });

  it('should retrieve the courseId private variable if not null and not call getItem', () => {
    localStorage.setItem('courseId', JSON.stringify(1));
    let actual = spectator.service.courseId;
    expect(spectator.service['_courseId']).toEqual(1);
    spies.getItem.mockRestore();
    actual = spectator.service.courseId;
    expect(spies.getItem).not.toHaveBeenCalled();
    expect(actual).toEqual(1);
  });

  it('should clear the state', () => {
    localStorage.setItem('sequence', JSON.stringify(mockGameSequence));
    localStorage.setItem('courseId', '1');
    spectator.service.sequenceState;
    spectator.service.courseId;
    expect(spectator.service['_sequenceState']).toEqual(mockGameSequence);
    expect(spectator.service['_courseId']).toEqual(1);
    spectator.service.clearSequenceState();
    expect(spies.removeItem).toHaveBeenCalledWith('sequence');
    expect(spies.removeItem).toHaveBeenCalledWith('courseId');
    expect(spectator.service['_sequenceState']).toBeNull();
    expect(spectator.service['_courseId']).toEqual(-1);
  });
});
