import { Injectable } from '@angular/core';
import { StateService } from '../shared/services/state.service';
import { take } from 'rxjs/operators';

import { GameApiService } from '@generated/gameservice/api/services/game-api.service';
import { GameSequence } from '@generated/gameservice/api/models';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private gameApiService: GameApiService, private stateService: StateService) {}

  loadSequence(): void {
    if (this.stateService.sequenceState) {
      this.stateService.newSequenceState$.next(this.stateService.sequenceState);
    } else {
      const courseId = this.stateService.courseId;
      this.gameApiService
        .getGameSequence({ courseid: courseId.toString() })
        .pipe(take(1))
        .subscribe((gameSequence) => {
          this.updateGame(gameSequence);
        });
    }
  }

  triggerNextFlashcard(): void {
    this.stateService.newSequenceState$.pipe(take(1)).subscribe((gameSequence: GameSequence) => {
      const newSequence = gameSequence.sequence?.splice(1);
      this.updateGame({ ...gameSequence, sequence: newSequence });
    });
  }

  private updateGame(newGameSequence: GameSequence): void {
    this.stateService.newSequenceState$.next(newGameSequence);
  }
}
