import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { of, ReplaySubject } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StateService } from '../shared/services/state.service';

import { GameApiService } from '@generated/gameservice/api/services/game-api.service';
import { GameSequence } from '@generated/gameservice/api/models';
import { GameService } from './game.service';

describe('GameService', () => {
  let spectator: SpectatorService<GameService>;

  const mockGameSequence: GameSequence = { courseid: '0', sequence: [3, 22, 53, 129] };
  const mockGameApiService = {
    getGameSequence: jest.fn().mockReturnValue(of(mockGameSequence)),
  };

  let mockSequenceState: any;
  let mockStateService = {
    newSequenceState$: new ReplaySubject<GameSequence>(1),
    courseId: jest.fn().mockReturnValue(0),
    sequenceState: mockSequenceState,
  };

  const createService = createServiceFactory({
    service: GameService,
    imports: [HttpClientTestingModule],
    providers: [
      { provide: GameApiService, useValue: mockGameApiService },
      { provide: StateService, useFactory: () => ({ ...mockStateService }) },
    ],
  });

  afterEach(() => {
    mockSequenceState = null;
  });

  it('should be created', () => {
    spectator = createService();
    expect(spectator.service).toBeTruthy();
  });

  it('calls the gameApiService if there is no sequence in the state', () => {
    spectator = createService();
    const mockGetGameSequence = jest.spyOn(spectator.service['gameApiService'], 'getGameSequence');
    spectator.service.loadSequence();
    expect(mockGetGameSequence).toHaveBeenCalled();
  });

  it('updates the state there is no sequence in the state', () => {
    spectator = createService();
    const spyNextStateService = jest.spyOn(spectator.service['stateService'].newSequenceState$, 'next');
    spectator.service.loadSequence();
    expect(spyNextStateService).toHaveBeenCalledWith(mockGameSequence);
  });

  it('uses the state if there is sequence in the state', () => {
    mockSequenceState = jest.fn().mockReturnValue(mockGameSequence);
    mockStateService = { ...mockStateService, sequenceState: mockSequenceState };
    spectator = createService();
    const mockGetGameSequence = jest.spyOn(spectator.service['gameApiService'], 'getGameSequence');
    spectator.service.loadSequence();
    expect(mockGetGameSequence).not.toHaveBeenCalled();
  });

  it('should pop the first number of sequence when triggering next flashcard', async () => {
    spectator = createService();
    spectator.service.loadSequence();
    spectator.service['stateService'].newSequenceState$.next(mockGameSequence);
    const spyNextStateService = jest.spyOn(GameService.prototype as any, 'updateGame');
    spectator.service.triggerNextFlashcard();
    expect(spyNextStateService).toHaveBeenCalledWith({ sequence: [22, 53, 129], courseid: '0' });
  });
});
