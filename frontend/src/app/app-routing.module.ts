import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/landing-page/landing-page.module').then((m) => m.LandingPageModule),
    pathMatch: 'full',
  },
  {
    path: 'flashcard',
    loadChildren: () => import('./modules//flashcard/flashcard.module').then((m) => m.FlashcardModule),
  },
  {
    path: 'page-not-found',
    loadChildren: () => import('./modules/page-not-found/page-not-found.module').then((m) => m.PageNotFoundModule),
  },
  {
    path: '**',
    redirectTo: '/page-not-found',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
