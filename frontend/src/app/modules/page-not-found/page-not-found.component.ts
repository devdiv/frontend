import { Component } from '@angular/core';
import { crashedStateTrigger } from './page-not-found-animations';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss'],
  animations: [crashedStateTrigger],
})
export class PageNotFoundComponent {
  constructor() {}
}
