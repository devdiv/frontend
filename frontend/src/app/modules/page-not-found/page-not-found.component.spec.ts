import { createRoutingFactory, SpectatorRouting } from '@ngneat/spectator';
import { Location } from '@angular/common';

import { LandingPageComponent } from '../landing-page/landing-page.component';
import { LandingPageModule } from '../landing-page/landing-page.module';
import { PageNotFoundComponent } from './page-not-found.component';

describe('PageNotFound Component', () => {
  let spectator: SpectatorRouting<PageNotFoundComponent>;
  const createComponent = createRoutingFactory({
    component: PageNotFoundComponent,
    imports: [LandingPageModule],
    stubsEnabled: false,
    routes: [
      {
        path: '',
        component: PageNotFoundComponent,
      },
      {
        path: 'flashcard',
        component: LandingPageComponent,
      },
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should navigate to / when the title section is clicked', async () => {
    expect(spectator.inject(Location).path()).toBe('/');

    spectator.click('.not-found-page__area__title');
    await spectator.fixture.whenStable();

    expect(spectator.inject(Location).path()).toBe('/');
  });
});
