import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '@mosar/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
  },
];

@NgModule({
  declarations: [LandingPageComponent],
  imports: [SharedModule, RouterModule.forChild(routes)],
})
export class LandingPageModule {}
