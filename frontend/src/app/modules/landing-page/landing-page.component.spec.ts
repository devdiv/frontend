import { createRoutingFactory, SpectatorRouting } from '@ngneat/spectator';
import { Observable, of } from 'rxjs';
import { Location } from '@angular/common';
import { SharedModule } from '@mosar/app/shared/shared.module';

import { Course } from '@generated/flashcardservice/api/models';
import { FlashcardComponent } from '../flashcard/flashcard.component';
import { FlashcardModule } from '../flashcard/flashcard.module';
import { FlashcardService } from '../flashcard/flashcard.service';
import { LandingPageComponent } from './landing-page.component';

describe('LandingPageComponent', () => {
  let spectator: SpectatorRouting<LandingPageComponent>;

  const coursesObs$: Observable<Course[]> = of([
    {
      courseid: 1,
      coursename: 'course One',
    },
    {
      courseid: 2,
      coursename: 'course Two',
    },
  ]);

  const mockFlashcardService: any = {
    getCourses: jest.fn().mockReturnValue(coursesObs$),
  };

  const createComponent = createRoutingFactory({
    component: LandingPageComponent,
    providers: [{ provide: FlashcardService, useValue: mockFlashcardService }],
    imports: [FlashcardModule, SharedModule],
    stubsEnabled: false,
    routes: [
      {
        path: '',
        component: LandingPageComponent,
      },
      {
        path: 'flashcard',
        component: FlashcardComponent,
      },
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should open the course modal and show the backdrop when the title section is clicked', async () => {
    spectator.click('.landing-page__area__title');
    await spectator.fixture.whenStable();
    expect(spectator.query('.landing-page__backdrop')).toBeVisible();
    expect(spectator.query('.course-modal__content')).toBeVisible();
    expect(spectator.query('.course-modal__content__link')).toBeVisible();
  });

  it('should call getCourses when the modal is opened', async () => {
    const mockGetCourses = jest.spyOn(spectator.component['flashcardService'], 'getCourses');
    spectator.click('.landing-page__area__title');
    await spectator.fixture.whenStable();
    expect(mockGetCourses).toHaveBeenCalledTimes(1);
  });

  it('should navigate to /flashcard when a menu item from the course modal is clicked', async () => {
    spectator.component.showCourseModal = true;
    spectator.detectChanges();
    expect(spectator.query('.course-modal__content__link')).toBeVisible();
    expect(spectator.inject(Location).path()).toBe('/');

    spectator.click('.course-modal__content__link');
    await spectator.fixture.whenStable();

    expect(spectator.inject(Location).path()).toBe('/flashcard');
  });
});
