import {
  animate,
  AnimationTriggerMetadata,
  group,
  keyframes,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const landingTitleStateTrigger: AnimationTriggerMetadata = trigger('landingTitleState', [
  transition(':enter', [
    animate(
      '1300ms linear',
      keyframes([
        style({
          opacity: 0,
          offset: 0,
        }),
        style({
          opacity: 0,
          offset: 0.7,
        }),
        style({
          opacity: 1,
          offset: 1,
        }),
      ]),
    ),
  ]),
  transition(
    ':leave',
    animate(
      '300ms ease-out',
      style({
        opacity: 0,
      }),
    ),
  ),
]);

export const landingStateTrigger: AnimationTriggerMetadata = trigger('landingState', [
  transition(':enter', [
    style({
      opacity: 0,
    }),
    group([
      animate(
        '700ms ease-in',
        style({
          opacity: 1,
        }),
      ),

      query(
        '.landing-page__container',
        animate(
          '1000ms linear',

          keyframes([
            style({
              opacity: 0,
              transform: 'rotateZ(0) scale(0.2)',
              color: 'transparent',
              offset: 0,
            }),
            style({
              opacity: 1,
              transform: 'rotateZ(-15deg) scale(2.5)',
              offset: 0.35,
            }),
            style({
              opacity: 1,
              transform: 'rotateZ(-40deg) scale(4)',
              offset: 0.6,
            }),
            style({
              opacity: 1,
              transform: 'rotateZ(-45deg) scale(5)',
              offset: 1,
            }),
          ]),
        ),
      ),
    ]),
  ]),
  transition(
    ':leave',
    animate(
      '300ms ease-out',
      style({
        opacity: 0,
      }),
    ),
  ),
]);
