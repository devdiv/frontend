import { BehaviorSubject, Observable } from 'rxjs';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { landingStateTrigger, landingTitleStateTrigger } from './landing-page.animations';
import { Router } from '@angular/router';

import { Course } from '@generated/flashcardservice/api/models';
import { FlashcardService } from '../flashcard/flashcard.service';
import { StateService } from '@mosar/app/shared/services/state.service';

@Component({
  selector: 'mosar-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  animations: [landingStateTrigger, landingTitleStateTrigger],
})
export class LandingPageComponent implements OnInit {
  @ViewChild('courseModal') courseModal: ElementRef = {} as ElementRef;
  titleHover = false;
  showCourseModal = false;
  animationState = '';
  loadingInProgress = false;
  courses$: Observable<Course[]> = new BehaviorSubject<Course[]>([]);

  constructor(private stateService: StateService, private router: Router, private flashcardService: FlashcardService) {}

  ngOnInit(): void {
    this.courses$ = this.flashcardService.getCourses();
  }

  navigate(course: Course): void {
    this.stateService.courseId$.next(course?.courseid);
    this.router.navigate(['/flashcard']);
  }

  onClick(event: Event): void {
    if (this.showCourseModal && event.target !== this.courseModal.nativeElement) {
      this.showCourseModal = false;
    }
  }
}
