import { createRoutingFactory, SpectatorRouting } from '@ngneat/spectator';
import { Observable, of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Location } from '@angular/common';

import { Flashcard } from '@mosar/../build/openapi/flashcardservice/api/models/flashcard';
import { FlashcardComponent } from './flashcard.component';
import { FlashcardService } from './flashcard.service';
import { GameSequence } from '@generated/gameservice/api/models';
import { GameService } from '@mosar/app/services/game.service';
import { LandingPageComponent } from '../landing-page/landing-page.component';
import { LandingPageModule } from '../landing-page/landing-page.module';
import { StateService } from '@mosar/app/shared/services/state.service';

describe('FlashcardComponent', () => {
  let spectator: SpectatorRouting<FlashcardComponent>;

  const flashcardObs$: Observable<Flashcard> = of({
    id: 'id-1',
    term: 'term',
    explanation: 'explanation',
  });

  const mockFlashcardService: any = {
    getFlashcard: jest.fn().mockReturnValue(flashcardObs$),
  };

  const mockGameSequence: GameSequence = {
    courseid: '0',
    sequence: [4, 3, 2, 1],
  };
  const mockGameService: any = {
    loadSequence: jest.fn().mockReturnValue(null),
    triggerNextFlashcard: jest.fn().mockReturnValue(null),
  };

  const mockStateService: any = {
    newSequenceState$: of(mockGameSequence),
    sequenceState: jest.fn(),
    clearSequenceState: jest.fn(),
  };

  const createComponent = createRoutingFactory({
    component: FlashcardComponent,
    imports: [LandingPageModule],
    providers: [
      { provide: FlashcardService, useValue: mockFlashcardService },
      { provide: GameService, useValue: mockGameService },
      { provide: StateService, useValue: mockStateService },
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    stubsEnabled: false,
    routes: [
      {
        path: '',
        component: LandingPageComponent,
      },
      {
        path: 'flashcard',
        component: FlashcardComponent,
      },
    ],
  });

  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should display only the term', () => {
    spectator.detectChanges();
    expect(spectator.query('.flashcard__content')?.textContent).toContain('term');
    expect(spectator.query('.flashcard__text-back')).toHaveClass('invisible');
    expect(spectator.query('.flashcard__text-front')).not.toHaveClass('invisible');
  });

  it('should display only the explanation after clicking on the card', () => {
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockFlipSides = jest.spyOn(spectator.component, 'flipSides');
    spectator.click('.flashcard__content');
    expect(spectator.query('.flashcard__content')?.textContent).toContain('explanation');
    expect(spectator.query('.flashcard__text-back')).not.toHaveClass('invisible');
    expect(spectator.query('.flashcard__text-front')).toHaveClass('invisible');
    expect(mockFlipSides).toHaveBeenCalled();
    expect(spectator.component.isFrontSide).toEqual(false);
  });

  it('should not display the buttons on the front side', () => {
    expect(spectator.component.isFrontSide).toEqual(true);
    expect(spectator.query('.flashcard__button__negative')).toBeNull();
    expect(spectator.query('.flashcard__button__positive')).toBeNull();
  });

  it('should display the buttons on the back side', () => {
    spectator.component.isFrontSide = false;
    spectator.detectChanges();
    expect(spectator.query('.flashcard__button__negative')).not.toBeNull();
    expect(spectator.query('.flashcard__button__positive')).not.toBeNull();
  });

  it('should call triggerAnimation if the current flashcard was marked correct', () => {
    spectator.component.isFrontSide = false;
    spectator.detectChanges();
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.click('mosar-button-positive');
    expect(mockTriggerAnimation).toHaveBeenCalledWith('right');
  });

  it('should call triggerAnimation if the current flashcard was marked incorrect', () => {
    spectator.component.isFrontSide = false;
    spectator.detectChanges();
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.click('mosar-button-negative');
    expect(mockTriggerAnimation).toHaveBeenCalledWith('left');
  });

  it('should flip side only if no animation is running', () => {
    spectator.component.animationInProgress = true;
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.flipSides();
    expect(spectator.component.isFrontSide).toEqual(true);
    expect(mockTriggerAnimation).not.toHaveBeenCalled();
  });

  it('should flip front to back and vice versa if no animation is currently running', () => {
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.flipSides();
    expect(mockTriggerAnimation).toHaveBeenCalledWith('back');
    spectator.component.flipSides();
    expect(mockTriggerAnimation).toHaveBeenCalledWith('front');
  });

  it('should set the animationState when triggerAnimation is called', () => {
    spectator.component.triggerAnimation('front');
    expect(spectator.component.animationState).toEqual('front');
  });

  it('should call triggerAnimation front in onDoneChooseNextAnimationState when animationstate is new', () => {
    spectator.component.animationState = 'new';
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.onDoneChooseNextAnimationState();
    expect(mockTriggerAnimation).toHaveBeenCalledWith('front');
    expect(spectator.component.animationInProgress).toEqual(false);
  });

  it('should call triggerAnimation new in onDoneChooseNextAnimationState when animationstate is left, and it is not the last', () => {
    spectator.component.animationState = 'left';
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.onDoneChooseNextAnimationState();
    expect(mockTriggerAnimation).toHaveBeenCalledWith('nothing');
    expect(spectator.component.animationInProgress).toEqual(true);
  });

  it('should call triggerAnimation new in onDoneChooseNextAnimationState when animationstate is right, and it is not the last', () => {
    spectator.component.animationState = 'right';
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.onDoneChooseNextAnimationState();
    expect(mockTriggerAnimation).toHaveBeenCalledWith('nothing');
    expect(spectator.component.animationInProgress).toEqual(true);
  });

  it('should navigate away in onDoneChooseNextAnimationState when animationstate is left, and it is the last', async () => {
    spectator.component.animationState = 'left';
    spectator.component.isLast = true;
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    const mockClearState = jest.spyOn(spectator.component['stateService'], 'clearSequenceState');
    spectator.component.onDoneChooseNextAnimationState();
    await spectator.fixture.whenStable();
    expect(mockTriggerAnimation).not.toHaveBeenCalled();
    expect(mockClearState).toHaveBeenCalled();
    expect(spectator.inject(Location).path()).toEqual('/');
  });

  it('should navigate away in onDoneChooseNextAnimationState when animationstate is right, and it is the last', async () => {
    spectator.component.animationState = 'right';
    spectator.component.isLast = true;
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.onDoneChooseNextAnimationState();
    expect(mockTriggerAnimation).not.toHaveBeenCalled();
    await spectator.fixture.whenStable();
    expect(spectator.inject(Location).path()).toEqual('/');
  });

  it('should not call triggerAnimation in onDoneChooseNextAnimationState when animationstate is front', () => {
    spectator.component.animationState = 'front';
    expect(spectator.component.isFrontSide).toEqual(true);
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.onDoneChooseNextAnimationState();
    expect(mockTriggerAnimation).not.toHaveBeenCalled();
    expect(spectator.component.animationInProgress).toEqual(false);
  });

  it('should not call triggerAnimation in onDoneChooseNextAnimationState when animationstate is back', () => {
    spectator.component.animationState = 'back';
    spectator.component.isFrontSide = false;
    spectator.detectChanges();
    const mockTriggerAnimation = jest.spyOn(spectator.component, 'triggerAnimation');
    spectator.component.onDoneChooseNextAnimationState();
    expect(mockTriggerAnimation).not.toHaveBeenCalled();
    expect(spectator.component.animationInProgress).toEqual(false);
  });

  it('should navigate to "/" when the home button is clicked', async () => {
    spectator.router.navigateByUrl('/flashcard');
    await spectator.fixture.whenStable();

    expect(spectator.query('.navigation-bar')).toBeVisible();
    expect(spectator.query('mosar-button-home')).toBeVisible();
    expect(spectator.inject(Location).path()).toBe('/flashcard');

    spectator.click('mosar-button-home');
    await spectator.fixture.whenStable();
    expect(spectator.inject(Location).path()).toBe('/');
  });
});
