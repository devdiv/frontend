import { Course } from '@generated/flashcardservice/api/models';
import { Flashcard } from '@generated/flashcardservice/api/models/flashcard';
import { FlashcardApiService } from '@generated/flashcardservice/api/services/flashcard-api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FlashcardService {
  constructor(private flashcardApiService: FlashcardApiService) {}

  getFlashcard(courseId: number, flashcardId: number): Observable<Flashcard> {
    return this.flashcardApiService.getFlashcard({
      courseid: courseId.toString(),
      flashcardid: flashcardId.toString(),
    });
  }

  getCourses(): Observable<Course[]> {
    return this.flashcardApiService.getCoursesAvailable();
  }
}
