import {
  animate,
  AnimationTriggerMetadata,
  group,
  keyframes,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const flashcardSideStateTrigger: AnimationTriggerMetadata = trigger('flashcardSideState', [
  transition(
    'front => back',
    group([
      query(
        '.flashcard__container',
        animate(
          '800ms ease-out',
          keyframes([
            style({
              transform: 'rotateY(0)',
              offset: 0,
            }),
            style({
              transform: 'rotateY(90deg)',
              offset: 0.499,
            }),
            style({
              transform: 'rotateY(-90deg)',
              offset: 0.5,
            }),
            style({
              transform: 'rotateY(0)',
              offset: 1,
            }),
          ]),
        ),
      ),

      query(
        '.flashcard__text-front',
        animate(
          '800ms ease-out',
          keyframes([
            style({
              opacity: 1,
              offset: 0,
            }),
            style({
              opacity: 1,
              offset: 0.4,
            }),
            style({
              opacity: 0,
              offset: 0.5,
            }),
            style({
              opacity: 0,
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.flashcard__text-back',
        animate(
          '800ms ease-out',
          keyframes([
            style({
              opacity: 0,
              offset: 0,
            }),
            style({
              opacity: 0,
              offset: 0.5,
            }),
            style({
              opacity: 1,
              offset: 0.6,
            }),
            style({
              opacity: 1,
              offset: 1,
            }),
          ]),
        ),
      ),
      query('.flashcard__button', [
        style({
          opacity: 0,
        }),
        animate(
          '500ms',
          style({
            opacity: 0,
          }),
        ),
        animate(
          '400ms ease-in',
          style({
            opacity: 1,
            transform: 'scale(1.12)',
          }),
        ),
        animate('150ms'),
      ]),
    ]),
  ),
  transition(
    'back => front',
    group([
      query(
        '.flashcard__container',
        animate(
          '800ms ease-out',
          keyframes([
            style({
              transform: 'rotateY(0)',

              offset: 0,
            }),
            style({
              transform: 'rotateY(-90deg)',
              offset: 0.498,
            }),
            style({
              opacity: 0,
              offset: 0.499,
            }),
            style({
              transform: 'rotateY(90deg)',
              offset: 0.5,
            }),
            style({
              opacity: 1,
              offset: 0.501,
            }),
            style({
              transform: 'rotateY(0)',
              offset: 1,
            }),
          ]),
        ),
      ),

      query(
        '.flashcard__text-back',
        animate(
          '800ms ease-out',
          keyframes([
            style({
              opacity: 1,
              offset: 0,
            }),
            style({
              opacity: 1,
              offset: 0.4,
            }),
            style({
              opacity: 0,
              offset: 0.5,
            }),
            style({
              opacity: 0,
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.flashcard__text-front',
        animate(
          '800ms ease-out',
          keyframes([
            style({
              opacity: 0,
              offset: 0,
            }),
            style({
              opacity: 0,
              offset: 0.5,
            }),
            style({
              opacity: 1,
              offset: 0.6,
            }),
            style({
              opacity: 1,
              offset: 1,
            }),
          ]),
        ),
      ),
      query('.flashcard__button', [
        style({
          opacity: 1,
        }),
        animate(
          '200ms ease-out',
          style({
            opacity: 0,
          }),
        ),
      ]),
    ]),
  ),
  transition(
    '* => new',
    group([
      query(
        '.flashcard__container',
        animate(
          '300ms',
          keyframes([
            style({
              transform: 'scale(0.9)',
              opacity: 0,
              offset: 0,
            }),
            style({
              transform: 'scale(1.02)',
              opacity: 0.7,
              offset: 0.4,
            }),
            style({
              transform: 'scale(1.01)',
              opacity: 1,
              offset: 0.6,
            }),
            style({
              transform: 'scale(1)',
              opacity: 1,
              offset: 1,
            }),
          ]),
        ),
      ),
    ]),
  ),
  transition(
    'back => right',
    group([
      query(
        '.flashcard__container',
        animate(
          '320ms ease-in',
          keyframes([
            style({
              transform: 'rotateZ(0) translateX(0) translate(0)',
              opacity: 1,
              offset: 0,
            }),
            style({
              transform: 'rotatez(30deg) translateX(100%) translateY(-85%)',
              opacity: 1,
              offset: 0.5,
            }),
            style({
              transform: 'rotatez(60deg) translateX(200%) translateY(-190%)',
              opacity: 0,
              offset: 1,
            }),
          ]),
        ),
      ),
    ]),
  ),
  transition(
    'back => left',
    group([
      query(
        '.flashcard__container',
        animate(
          '320ms ease-in',
          keyframes([
            style({
              transform: 'rotateZ(0) translateX(0) translate(0)',
              opacity: 1,
              offset: 0,
            }),
            style({
              transform: 'rotatez(-30deg) translateX(-100%) translateY(-85%)',
              opacity: 1,
              offset: 0.5,
            }),
            style({
              transform: 'rotatez(-60deg) translateX(-200%) translateY(-190%)',
              opacity: 0,
              offset: 1,
            }),
          ]),
        ),
      ),
    ]),
  ),
]);

export const flashcardNextStateTrigger: AnimationTriggerMetadata = trigger('flashcardNextState', []);
