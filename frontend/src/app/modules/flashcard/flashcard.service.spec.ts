import { createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { FlashcardApiService } from '@mosar/../build/openapi/flashcardservice/api/services/flashcard-api.service';
import { FlashcardService } from './flashcard.service';
import { of } from 'rxjs';

describe('FlashcardService', () => {
  let spectator: SpectatorService<FlashcardService>;

  const mockFlashcard = {
    id: '1',
    term: 'this term',
    explanation: 'is well explained',
  };
  const mockFlashcardApiService = {
    getFlashcard: jest.fn().mockReturnValue(of(mockFlashcard)),
  };
  const createService = createServiceFactory({
    service: FlashcardService,
    imports: [HttpClientTestingModule],
    providers: [{ provide: FlashcardApiService, useValue: mockFlashcardApiService }],
  });

  beforeEach(() => {
    spectator = createService();
  });

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });
  it('should call and get a result from the flashcardApiService', async () => {
    spectator.service.getFlashcard(0, 1).subscribe((result) => {
      expect(spectator.service['flashcardApiService'].getFlashcard).toHaveBeenCalledWith({
        courseid: '0',
        flashcardid: '1',
      });
      expect(result).toEqual(mockFlashcard);
    });
  });
});
