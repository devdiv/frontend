export type FlashcardAnimationState = 'front' | 'back' | 'left' | 'right' | 'new' | 'nothing';
